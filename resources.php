<?php
$to = cs_var('page_parameter1') ? cs_var('page_parameter1') : false;

$links = [
	'work-wanted' => 'https://docs.google.com/document/d/1CfwZYvH98QRMdvRzLTURLb1L7Iyl43Ri2mgwyqc_Rho/|Work Required at Sri Aurobindo Dhama',
	'supraja-presentation' => 'https://docs.google.com/presentation/d/13CgaDS-fE9Xi4j8yx_eTxCTnS7aINO6HOuj-UfloARc/edit?usp=sharing|Supraja Project Presentation',
	'talk-to-us' => 'https://community.yieldmore.org/user/sri_aurobindo_dhama|Our User Profile on the YieldMore.org Community',
];

$go = cs_var('node') == 'go';

if ($go && $to && isset($links[$to])) {
	$link = explode('|', $links[$to])[0];
	header('Location: ' . $link);
	return;
}

if ($go) { echo 'No GO link defined, pls visit <a href="../resources/">our resources page</a>.'; return; }
?>
			<div class="content-wrap pt-lg-0 pt-xl-0 pb-0">

				<div class="container clearfix">

					<div class="heading-block border-bottom-0 center pt-4 mb-3">
						<h3>History</h3>
					</div>
<?php
echo '<ol>';
foreach ($links as $slug=>$url_name) {
	$bits = explode('|', $url_name);
	echo sprintf('<li><a href="%s" target="_blank" title="%s">%s</a></li>', cs_var('url') . 'go/' . $slug . '/', $bits[0], $bits[1]);
}
echo '</ol>';
?>

				</div>
			</div>
