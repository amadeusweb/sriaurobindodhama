<?php
$txts = scandir('./content/');
usort($txts, "strnatcmp"); //https://www.php.net/manual/en/function.strnatcmp.php

echo sprintf('<h1>%s</h1><br />%s<br/>BYLINE: %s<br/>MESSAGE: %s<hr />', am_var('name'), am_var('url'), am_var('byline'), am_var('footer-message'));

echo sprintf("\r\n\r\n" . '<a href="%s"><img src="%slogo-%s-rectangle.png" alt="%s" class="img-fluid" /></a><br /><br />', am_var('url'), am_var('url'), am_var('safeName'), am_var('safeName'));

foreach ($txts as $txt) {
	if (!contains($txt, '.txt')) continue;
	echo '<hr />';
	$slug = str_replace('.txt', '', $txt);
	echo sprintf('<h2>%s</h2><br />%s', $slug, am_var('url') . $slug . '/');
	echo wpautop(file_get_contents('./content/' . $txt));
}

?>
