			<div class="content-wrap pt-lg-0 pt-xl-0 pb-0">

				<div class="container clearfix">

					<div class="heading-block border-bottom-0 center pt-4 mb-3">
						<h3>Spiritual Communes or Deva Sanghas</h3>
					</div>

<blockquote>Sri Aurobindo first used the name "Deva Sanghas" for the spiritual collectives.</blockquote>

Sri Aurobindo envisioned a sacred place for Spiritual  seekers to come together form a community and  live, work, pray together for collective divinisation.

In ancient and medieval India, villages were self-sufficient in meeting the needs of each village except salt and things like that. 64 vidyas/skills/kalas were practiced and they were content, peaceful and administered by their own men called panchayats and each village was like a miniature country. To meet one's needs, barter system was in existence in place of money. Caring and sharing was the general tendency.

Economy was based on bounties of Mother nature - agriculture, wealth of domesticated animals, forest produces. In the present scenario, political parties have infiltrated into village panchayats that are  now corrupted and polluted.

Prior to 1950s man found ready harmony in rural settings when he enjoys the gathering of the bounties of Nature devoid of present day cut throat competition. Nature showers bounties to meet every one's needs but not greed. This was the secret of healthy, happy and peaceful life of our ancestors who were generous in terms of human values, though not flush with material wealth. They led simple living and pious  life. In an attempt to find out an alternate life pattern as opposed to the present day cut throat competition, adulteration, vulgarization, criminalization, complexities of human relations, pollution, insensitivities towards ecology & environment, dehumanization, we are sketching out an alternate life in the light of teachings of Sri Aurobindo and the Mother. Instead of the cultural and spiritual life dominating the Indian scenario, at present consumerism is dominating every walk of life.

TWO TRENDS OF NATURE - INDIVIDUAL AND COLLECTIVE

The present civilization is individual oriented. Individuals have developed his multifaceted personality. All the components of the individuality has developed to a certain extent. Till now the individual has been dominating the world scene. Now the time has come in the evolutionary march of man for the Nature to take up another trait - the collective divinisation. Nature has been consolidating Her gains by grooming and shaping individuals till now. 

Now Nature wants to start collectivity on a considerable scale. She has taken up her second trend - the formation and the development of the collective unit. The present unit of society is the family which consists of husband, wife and a couple of children. The family is not the true collective unit in view of the second trait of evolving Nature. The family is formed partly for the perpetration of the race. The Individual work hard day and night in order to rear children. All his efforts and energies are centred round his family. According to the second trait of Nature, the individual has to reflect and manifest the real collective spirit which is full of love for others - 'Vasudeva Kutumbakam'. The saints' aashrams are true collectives. Among aashrams, there are two types - one, the ashram of a realised soul like Ramana Maharshi, the other of unrealized swamiji's. The swamiji's are on the way to realisation and are not yet realised.

Nature always works out her plan through the developed and evolved individuals. The collective trend is definitely a spiritual one. Natures first individual based civilizations innings is over. She has taken up the second innings of collective living. Certain rare and spiritual individuals are manifesting their cooperative quality of living in certain enclave or Deva sanghas. 

Deva Sangha are not for the unevolved common man. They are meant for spiritual elites known as pioneers. True collectivism is always spiritual experiment and now the time has come for the world to enter into a true subjective age.

Predicament of the present world
A new civilization is in the offing. The present civilization has become spent out and bankrupt. Anytime it can collapse like a house of cards. Science instead of enriching the civilization has become the factor for disintegration. Shark materialism is reigning supreme and controlling the reins of civilization. The present civilization has thrown up huge super structures in every field. The individual has become a non entity. There are yet rare spiritual individuals but no one is caring their teachings. Nature has already begun preparing the next alternative. Already signs of a new millennium are appearing on the horizon. Sri Aurobindo has already given us hints in "The life divine", "The Human Cycle" and "Ideal of human unity". All philosophies are for practice. All philosophies are born to change the nature of man, transform man into a spiritual being. Nature as it is, has entrusted the welfare of the outer needs to the West and the the subject of inner needs to the East. Now in future, the two needs should be integrated in Deva sanghas.
What are Deva Sanghas and what are their main features

They are spiritual enclaves. Their features are:

1. Out of inner need, they come together and live in rural spiritual enclave.
2. They are for total de-urbanization with some exception. Sanghas are primarily rural collectives.
3. Members, who have an altogether enlightened view of the private property, live in Deva sanghas. All property belongs to the divine. We are all his children. So the untold riches of Nature should be utilised for the the material as well as the spiritual development of all. Among the children of the divine, some are more intelligent than others. Naturally the management of the riches of nature will be entrusted to the more interasted and capable person. Deva sangha members are trustees rather than owners.
4. At present money is in the hands of the Anti -  Divine forces. Misuse of money on a large scale is going on. Members of the deva sanghas want to reclaim the riches of nature from the Anti - Divine forces.
5. More than all, the members of the Deva Sangha don't want to live as isolated individuals; but they want to live as a group, as a collective. According to Sri Aurobindo, the second cosmic dimension of the individual soul is sprouting. Naturally such a universal-cum-individual by nature, would like to live in collectives. The sprouting of the cosmic dimension compels the individual to live in Deva sanghas.
6. In Deva sanghas, there will be no difference between the helpers (servants) and the trustees. The sons and daughters would be employed in the various departments of Deva sanghas.
7. In the beginning all the members of the Deva sanghas may not be ready to surrender their property to the Deva Sangha Trusts. So there would be several categories of the membership. Primarily all agree to live in Deva sanghas in the rural areas and not in the urban areas. They can start separate business concerns and contribute a substantial portion of the annual profits to the Deva sanghas. No hard and fast rule is there. But all without exception would love and live in Deva Sanghas. They would enjoy collective living.
8. One can remain celibate, marry, or a member may have his family consisting of three generations. He will love his family as well as the families living in Dev Sangha. At least now the time has come for the earnest devotees to live in Deva Sanghas. They can start their own rural/cottage industries, hence no need to seek any government or private jobs.
9. The son and daughter of the members of Dev Sangha will be educated in homeschooling in an ideal environment at  Deva Sanghas.
10. Deva sanghas is committed to take care of the needs of the members till they live as in any family.

Differences between the existing Swamijis Ashramas and Deva Sanghas

At present in all the swamijis' Ashramas, there would be one owner-swamiji and the rest his disciples. The disciples owe their allegiance to the Owner-Swamiji. He is not one among the equals. The individuality of the disciple is not taken into account. Whereas in all the  Deva sanghas even the most evolved is one among the equals.

A new economy is worked out in Deva Sanghas
Deva Sanghas will control the money. If the spiritual minded takes up the management of production of  agri products, dairy products, other related products, processing of the same, packages and distribution, adulteration becomes impossible and this influences the society. There are lakhs of spiritual minded devotees in India belongs to several denomination to take up various resposibilities  in Deva Sanghas. Money should  be utilised for the welfare of all the members.

Training of apprentice
During vacations in schools and colleges, Deva Sanghas open their doors to the interested students to learn any job skill to make him Aatma Nirbhara in a disciplined and responsible environment. Deva Sanghas will provide free lodging and boarding facilities.


Introduction of place where Deva Sangha is being planned to start
90 kilometre from Bangalore is Sri Aurobindodhama situated on the bank of river Cauvery amidst forest, surrounded by hills. Sri Aurobindodhama is a charitable trust have built meditation hall, library, yoga hall, sadhaka kutirs-7 no. with community kitchen. Attached to this are Divine parks, kitchen garden, 12 number of sacred vanas in 14 acres of land. Our neighbour is Om Shantidhama charitable trust running vedik gurukula and sheltered brahmacharya, gruhastha, vana prastha and sanyasa ashrama in 33 acres. Sri Aurobindodhama trustees are also in Om Shantidham trust. There is a residential layout in which Sri Aurobindodhama has a few sites meant for members of Deva sangha to build their own house with no consideration of money for the use of sites. Some houses are also available to be spared and dormitories for students are available.

<a href="../activities/">See all activities that can be begun now</a>

Target group:
Deva Sangha is looking for three kinds of fortunes amongst the prospective members who are spiritually oriented - material wealth, intellectual wealth and affection wealth.
There are lakhs and lakhs of young people who have materialized their worldly gains beyond their own expectations & have no more inclination for any further worldly pursuits. When the blank future stares at them, evolved souls amongst them find refuge in spiritual pursuit that opens vista for meaningful employment of their remaining life.
Individuals aspiring for inner quest in spiritual life devoid of any materialistic desires.  The Deva Sangha invites evolved people .
Aspiring to shed ego by subjecting to transformation of ones lower nature and always open to the higher forces that heightens, widens and deepens the consciousness.                 

				</div>
			</div>
