# Journal

Is the record of learnings and observations by ANY / ALL members of the Deva Sangha

# Seeking

A "diary of the many seekers", this section chronicles discussions and "Satsang in a Circle" (see post by Imran)

# Direction

Plans, Ambitions, Dreams

# Library

Catalogues - available only on campus

# Guestbook

[files structure as: year/month-occasion.md]

# Alliance

Mention of other Aurobindonian and Spiritually Awakened Organizations with details about our learning from and interactions with them.

[To be linked to from our library]
