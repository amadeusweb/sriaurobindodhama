# INSIDE LEFT

The Sri Aurobindo Dhama Experience

1. Living with Nature

2. Gurukula, Forest Life and Farming

3. Perspectives on City and Family Life

4. Silence, Contemplation, Self Study and Inner Development

5. Practices, Sadhana and Spiritual Awakening

6. Devasangha, Spiritual Commune and Community Living

7. Learning from the Masters, Awakening the Spiritual Fire and Accelerated Evolution
