<section id="cover">
	<h2>ONE DAY</h2>
	<h1>RETREAT WITH NATURE</h1>
	<h3>AT AUROGRAM, SANGAMA</h3>
</section>

<section id="picture">
	<img src="../../assets/aurogram.jpg" style="width: 40%;" />
</section>

<section id="purpose">
	<h2>MAIN PURPOSE</h2>
	<h1>STAY | SATVIK FOOD | NATURE</h1>
	<h3>group bonding</h3>
</section>

<section id="details">
	<h2>@ Aurogram</h2>
	<h1>Sri Aurobindo Dhama</h1>
	<h3>Sangama, Karnataka</h3>
	<h3>October __ 2022</h3>
</section>

<section id="morning-session">
	<h2>MORNING SESSION</h2>
	<ul style="text-align: center; list-style-type: none;">
		<li>Guided Meditation - 5.30am</li>
		<li>Yoga & Pranayama - 6am</li>
		<li>Yoga Nidra - 7.30am</li>
		<!--8 to 9 - Breakfast-->
		<li>Talk on "Nature" - 9.45am</li>
		<!--10.30 to 11 - Tea Break-->
		<li>Talk on "How to live with Nature" - 11am</li>
	</ul>
</section>

<section id="afternoon-session">
	<h2>AFTERNOON and EVENING SESSION</h2>
	<ul style="text-align: center; list-style-type: none;">
		<li>Intimate Gatherings Workshop- 2.30pm</li>
		<li>Garden Bonding and Reflections - 4.30pm</li>
		<!--Tea Break-->
		<li>Creating Sankalp and Affirmations - 5pm</li>
		<li>Silence Meditation - 6pm</li>
		<li>Bonfire at Indraprastha - 7pm</li>
	</ul>
</section>

<section id="call-to-action">
	<h2>[MORE DETAILS]</h2>
	<h1>[CALL / RSVP]</h1>
	<h3>[SHARE BUTTON]</h3>
</section>
