<?php

//used in menu
function active_if($node) {
	$folder = false; //TODO: am_var('safeFolder') == $node.substring(1)
	if (am_var('node') == $node || $folder) echo ' active';
}

function before_render() {
	if (am_var('node') == 'present') {
		am_var('deck', SITEPATH . '/decks/' . am_var('page_parameter1') . '.md');
		am_var('embed', true);
		return;
	}

	if (am_var('node') == 'go') { include_once 'resources.php'; exit; }
	if (am_var('node') == 'print') { include_once 'print.php'; exit; }
}

function did_render_page() {
	if (am_var('deck')) {
		load_amadeus_module('revealjs');
		return true;
	}
}

function before_file() {?>
		<!-- Content
		============================================= -->
		<section id="content" class="bg-light">
<?php
}

function after_file() {
	run_site_code('footer-content');
	echo '</section>';
}
function random_quote() {
	$nl = am_var('nl');

	$cols = 'object';
	$items = tsv_to_array(file_get_contents(__DIR__ . '/../data/quotes.tsv'), $cols);
	$quote = $items[array_rand($items, 1)];

	if ($id = isset($_GET['quote']) ? $_GET['quote'] : false) {
		foreach ($items as $q) {
			if ($id == $q[$cols->SNo]) {
				$quote = $q;
				break;
			}
		}
	}

	$m_cols = 'object';
	$m_items = tsv_to_array(file_get_contents(__DIR__ . '/../data/quotes-macros.tsv'), $m_cols);
	$macro = false;

	foreach ($m_items as $m) { if (trim($m[$m_cols->Key]) == trim($quote[$cols->Macro])) { $macro = $m; break; } }
	$link = $macro ? sprintf('&nbsp;&nbsp;|&nbsp;&nbsp;<a href="%s" target="_blank">%s</a>', replace_vars($macro[$m_cols->Link]), $macro[$m_cols->Text]) : '';
	$more = $quote[$cols->More];

	$html = '<div class="quote-above-banner quote"><u>RANDOM QUOTE</u>: '. $nl;
	$html .= $quote[$cols->What]. $nl;
	$html .= ($more ? ' <a href="#" class="toggle-more">&hellip;</a><div class="mode hidden">' . $more . $link . '</div>' : ''). $nl;
	$html .= '</div>'. $nl;
	echo $html;
}
?>
