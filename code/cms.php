<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

include_once 'functions.php';

am_var('local', $local = startsWith($_SERVER['HTTP_HOST'], 'localhost'));

bootstrap(array(
	'name' => 'Sri Aurobindo Dhama',
	'safeName' => 'sriaurobindodhama',

	'byline' => 'Precipitating Growth',
	'footer-message' => 'A place for the seeker to unfold inner wisdom under the umbrella of Aurobindonian Philosophy and Teachings.',
	'start_year' => 2012,

	'version' => [ 'id' => '002', 'date' => '27 Nov 2021' ],

	'support_page_parameters' => true, //NB: For go
	'uses' => 'search1, social1, footer-message', //TODO: turn these back on
	'dont-wrap-menu-in-ul' => true,
	'menu_active_class' => 'active', //TODO: 

	//'robots' => 'noindex',
	'theme' => 'cv-modern-blog', // 'tm-xtra',
	'original_theme' => 'tm-xtra',
	'folder' => 'content/',

	'styles' => ['styles'],
	'scripts' => ['main'],
	//TODO: Analytics ID! 'head_hooks' => [__DIR__ . '/_ga.php'],
	'url' => $local ? replace_vars('http://localhost%port%/showcase/sriaurobindodhama/', 'port') : 'https://showcase.amadeusweb.com/sriaurobindodhama/',
	'path' => SITEPATH,
	'no-local-stats' => true,
));

render();
?>
