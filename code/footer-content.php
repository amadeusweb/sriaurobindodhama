<hr />
<div class="row footer-content">
    <div class="col-md-4 col-sm-12">
        <?php echo sprintf('<a href="%s"><img src="%slogo-%s-rectangle.png%s" alt="%s" class="img-fluid img-margin" /></a>', am_var('url'), am_var('url'), am_var('safeName'), version(), am_var('safeName')) ?>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="text-v-center">
          <b><?php echo am_var('name'); ?></b><br />
          <em><?php echo am_var('byline'); ?></em>
        </div>
    </div>
</div>
<?php random_quote(); ?>
