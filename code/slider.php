		<!-- Slider
		============================================= -->
		<section id="slider" class="slider-element min-vh-60 min-vh-md-100">
			<div class="fslider h-100 position-absolute" data-speed="1800" data-pause="5000" data-animation="fade" data-arrows="true" data-pagi="false">
				<div class="flexslider">
					<div class="slider-wrap">
<?php
$node = am_var('node');
$slideNames = [
	'sa-young' => [ 'text' => 'Sri Aurobindo', 'tagline' => 'Saint and Visionary, Ashram Founder', 'file' => 'sri-aurobindo-1915', 'css-pos' => 'top center', 'href' => '%url%sri-aurobindo/' ],
	'ma-young' => [ 'text' => 'Mirra Alfassa (The Mother)', 'tagline' => 'Spiritual Master, Founder of Auroville', 'file' => 'divine-mother-in-japan', 'css-pos' => 'top center', 'href' => '%url%mirra-alfassa/' ],
	'welcome' => [ 'text' => 'Sri Aurobindo Dhama', 'tagline' => 'Precipitating Growth', 'file' => 'sabd-welcome', 'href' => '%url%aims-and-objectives/' ],
	'auroville' => [ 'text' => 'Auroville, South India', 'tagline' => 'Experimental Township, Our Spiritual Center', 'file' => 'auroville', 'href' => 'https://auroville.org/' ],
	'conception' => [ 'text' => 'Conscious Conception', 'tagline' => 'The doorway to having good progeny', 'file' => 'sabd-conception', 'css-pos' => 'bottom center', 'href' => '%url%conscious-conception/' ],
	//'' => [ 'text' => '', 'tagline' => '', 'file' => '', 'href' => '%url%/' ],
];
$slides = ['sa-young', 'ma-young', 'welcome'];
if ($node == 'conscious-conception' || $node == 'good-progeny-or-supraja') $slides = ['conception', 'welcome'];
if ($node == 'philosophy') $slides = ['sa-young', 'auroville'];

foreach ($slides as $key) {
$slide = $slideNames[$key];
$img = am_var('url') . 'assets/slider/' . $slide['file'] . '.jpg';
$pos = isset($slide['css-pos']) ? $slide['css-pos'] : 'center center';
$link = replace_vars($slide['href']); if (strpos($link, 'http')) $link .= '" target="_blank';

?>
						<div class="slide text-center" style="background: url('<?php echo $img; ?>') <?php echo $pos; ?>; background-size: cover;">
							<div class="bg-overlay">
								<div class="bg-overlay-content align-items-end">
									<div class="slide-caption px-4 pb-4 dark">
										<h3 class="mb-2 h1"><a href="<?php echo $link; ?>" class="text-white"><?php echo $slide['text']; ?></a></h3>
										<p class="h5"><?php echo $slide['tagline']; ?></p>
									</div>
								</div>
							</div>
						</div>
<?php } ?>
					</div>
				</div>
			</div>
		</section><!-- #Slider end -->
